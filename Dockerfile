FROM openjdk:17-alpine
COPY ${JAR_FILE} app.jar
ARG JAR_FILE=build/libs/demo2-0.0.1-SNAPSHOT.jar
CMD ["java" , "-jar" , "/app.jar"]