package com.example.demo.Exception;

public class PasswordException extends RuntimeException{
    public PasswordException(String message) {
        super(message);
    }
}
