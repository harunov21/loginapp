package com.example.demo.Repo;

import com.example.demo.Model.Users;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users,Long> {
    Optional<Users> findByEmail(String email);

    @Query(value = "select * from users where users.email=:emails", nativeQuery = true)
    Optional<List<User>> findByEmails(String emails);
}