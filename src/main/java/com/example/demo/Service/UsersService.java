package com.example.demo.Service;

import com.example.demo.Exception.UserNotFoundException;
import com.example.demo.dto.LoginUserRequestDto;
import com.example.demo.dto.UserRequestDto;

import javax.security.auth.login.LoginException;

public interface UsersService {
    Long save(UserRequestDto userRequestDto);

    String loginUser(LoginUserRequestDto logindto) throws UserNotFoundException;

    String loginUser2(LoginUserRequestDto loginUserRequestDto);
}
