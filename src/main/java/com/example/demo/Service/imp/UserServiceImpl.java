package com.example.demo.Service.imp;

import com.example.demo.Config.UserConfiguration;
import com.example.demo.Exception.PasswordException;
import com.example.demo.Exception.UserNotFoundException;
import com.example.demo.Model.Users;
import com.example.demo.Repo.UsersRepository;
import com.example.demo.Service.UsersService;
import com.example.demo.dto.LoginUserRequestDto;
import com.example.demo.dto.UserRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.security.auth.login.LoginException;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
@Slf4j
public class UserServiceImpl implements UsersService {
    private final UsersRepository usersRepository;
    private final UserConfiguration userConfiguration;
    @Override
    public Long save(UserRequestDto userRequestDto) {
        Users users = userConfiguration.getMapper().map(userRequestDto, Users.class);
        String password = userRequestDto.getPassword();
        if (password.length() < 8) {
            throw new PasswordException("Password must be at least 8 characters long.");
        }
        return usersRepository.save(users).getId();
    }


    @Override
    public String loginUser(LoginUserRequestDto logindto) throws UserNotFoundException {
        Optional<Users> optionalUsers = usersRepository.findByEmail(logindto.getEmail());
        optionalUsers.ifPresent(users -> users.setPassword(logindto.getPassword()));
        if (optionalUsers.isPresent()) {
            Users user = optionalUsers.get();
            if (!user.getPassword().equals(logindto.getPassword())) {
                throw new UserNotFoundException("Invalid password for the provided email");
            }
        } else {
            throw new UserNotFoundException("User with the provided email not found.");
        }
        return "Login successfully";

    }

    @Override
    public String loginUser2(LoginUserRequestDto loginUserRequestDto) throws PasswordException {
        Optional<List<User>> optionalUser = usersRepository.findByEmails(loginUserRequestDto.getEmail());
        log.info("Get users: {}", optionalUser.get());
        optionalUser.ifPresent(users -> {
                    for (User user : users) {
                        if (!user.getPassword().equals(loginUserRequestDto.getPassword())) {
                            throw new PasswordException("Password is incorrect");
                        }
                    }
                }
        );
        return null;
    }
}
