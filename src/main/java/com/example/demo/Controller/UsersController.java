package com.example.demo.Controller;

import com.example.demo.Exception.UserNotFoundException;
import com.example.demo.Service.UsersService;
import com.example.demo.dto.LoginUserRequestDto;
import com.example.demo.dto.UserRequestDto;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.login.LoginException;

@RequiredArgsConstructor
@RequestMapping("/users")
@RestController
public class UsersController {

    private final UsersService usersService;


   @PostMapping("/create")
    public String createUser(@Valid @RequestBody UserRequestDto userRequestDto){
         usersService.save(userRequestDto);
         return "User created";
   }

   @PostMapping("/login")
    public String loginUser (@Valid @RequestBody LoginUserRequestDto loginDto) throws UserNotFoundException, LoginException {
      return usersService.loginUser(loginDto);


   }
    @PostMapping("/login2")
    public String loginUser2(@RequestBody LoginUserRequestDto loginUserRequestDto) {
        return usersService.loginUser2(loginUserRequestDto);
    }

}
