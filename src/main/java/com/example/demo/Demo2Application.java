package com.example.demo;

import com.example.demo.Model.Users;
import com.example.demo.Repo.UsersRepository;
import com.example.demo.Service.UsersService;
import com.example.demo.dto.UserRequestDto;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@RequiredArgsConstructor
@SpringBootApplication
public class Demo2Application implements CommandLineRunner {

    private final UsersService usersService;

    public static void main(String[] args) {
        SpringApplication.run(Demo2Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        usersService.save(UserRequestDto.builder()
                        .name("Kamil")
                        .email("harunov@gmail.com")
                        .password("12345678")
                        .build());



    }
}
