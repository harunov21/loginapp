package com.example.demo.Config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ui.ModelMap;

@Configuration
public class UserConfiguration {
    @Bean
    public ModelMapper getMapper (){
        return new ModelMapper();
    }
}
